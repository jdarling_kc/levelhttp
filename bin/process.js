var fs = require('fs');
var Hapi = require('hapi');
var util = require('util');
var path = require('path');
var config = require('../lib/config').config;
var apiConfig = config.api;
var routesPath = path.resolve(config.routesPath||'./routes/');

var PORT = config.web.port || 9291;
var server = new Hapi.Server(PORT);

fs.readdir(routesPath, function(err, files){
  if(err){
    console.log(err);
  }else{
    var i=0, l=files.length, fileName, Router;
    var reIsJSFile = /\.js$/i;
    for(i=0; i<l; i++){
      fileName = files[i];
      if(fileName.match(reIsJSFile) ){
        try{
          var section = fileName.replace(/\.js$/i, '').toLowerCase();
          var cfg = config[section]||{};
          cfg.route = cfg.route || apiConfig.route;
          console.log('Loading module: ', fileName);
          require('../routes/'+fileName)(server, cfg);
        }catch(e){
          console.log('Error loading: '+fileName);
          console.log(e);
        }
      }
    }
  }
});

var started = function(){
  console.log("Server started on http://localhost:%s", PORT);
};

server.start(started);
