/*
  Store(name)
    get(id, callback)
    insert(record, callback)
    update(id, record, callback)
    asArray(options, callback)
      options{
        offset: Number
        limit: Number
        filter: Object
        sort: {
          Key: Direction(1 Ascending, -1 Descending)
          ...
        }
      }
    ensure(record, callback)
*/

var config = require('../lib/config');
var _stores = {};
var Store = require('../lib/leveldbstore');
var logger = require('../lib/logger');

var loadStore = function(collectionName){
  return new Store(collectionName);
};

var getStore = module.exports = function(collectionName){
  return _stores[collectionName] || (_stores[collectionName] = loadStore(collectionName));
};

module.exports.Store = Store;