var logger = require('../lib/logger');
var stores = require('../lib/store');

var listRecords = function(request, reply){
  var self = this, records = [];
  stores.Store.db().createReadStream({
  })
  .on('data', function(data){
    records.push(data);
  })
  .on('error', function(err){
    return reply(err);
  })
  .on('close', function(){
    return reply(records);
  });
};

var getRecord = function(request, reply){
  stores.Store.db().get(request.params.key, function(err, curr){
    reply(err||curr);
  });
};

var setRecord = function(request, reply){
  stores.Store.db().put(request.params.key, request.payload, function(err){
    if(err){
      return reply(err);
    }
    return reply(null, {key: request.params.key, value: request.payload});
  });
};

var deleteRecord = function(request, reply){
  stores.Store.db().del(request.params.key, function(err){
    reply(err||'Ok');
  });
};

module.exports = function(server, config){
  server.route([
    {
      method: 'GET',
      path: config.route+'keystore',
      handler: listRecords
    },
    {
      method: 'GET',
      path: config.route+'keystore/{key}',
      handler: getRecord
    },
    {
      method: 'POST',
      path: config.route+'keystore/{key}',
      handler: setRecord
    },
    {
      method: 'PUT',
      path: config.route+'keystore/{key}',
      handler: setRecord
    },
    {
      method: 'DELETE',
      path: config.route+'keystore/{key}',
      handler: deleteRecord
    },
  ]);
};