var logger = require('../lib/logger');
var stores = require('../lib/store');

var listRecords = function(request, reply){
  stores(request.params.name).asArray(request.query, function(err, response){
    reply(err||response);
  });
};

var getRecord = function(request, reply){
  stores(request.params.name).get(request.params.id, function(err, response){
    reply(err||response);
  });
};

var createRecord = function(request, reply){
  stores(request.params.name).insert(request.payload, function(err, response){
    reply(err||response);
  });
};

var updateRecord = function(request, reply){
  stores(request.params.name).update(request.params.id, request.payload, function(err, response){
    reply(err||response);
  });
};

var deleteRecord = function(request, reply){
  stores(request.params.name).delete(request.params.id, function(err, response){
    reply(err||response);
  });
};

var getAll = function(request, reply){
  var self = this, records = [];
  stores.Store.db().createReadStream({
  })
  .on('data', function(data){
    records.push(data);
  })
  .on('error', function(err){
    return reply(err);
  })
  .on('close', function(){
    return reply(records);
  });
};

module.exports = function(server, config){
  server.route([
    {
      method: 'GET',
      path: config.route+'table/{name}',
      handler: listRecords
    },
    {
      method: 'GET',
      path: config.route+'table/{name}/{id}',
      handler: getRecord
    },
    {
      method: 'POST',
      path: config.route+'table/{name}',
      handler: createRecord
    },
    {
      method: 'PUT',
      path: config.route+'table/{name}',
      handler: createRecord
    },
    {
      method: 'POST',
      path: config.route+'table/{name}/{id}',
      handler: updateRecord
    },
    {
      method: 'PUT',
      path: config.route+'table/{name}/{id}',
      handler: updateRecord
    },
    {
      method: 'DELETE',
      path: config.route+'table/{name}/{id}',
      handler: deleteRecord
    },
  ]);
};